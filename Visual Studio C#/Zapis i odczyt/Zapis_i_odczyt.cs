﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace WindowsFormsApp4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            String tekst;
            StreamWriter plik;
            plik = new StreamWriter("plik.txt");
            tekst = richTextBox1.Text;
            plik.Write(tekst);
            plik.Close();
            MessageBox.Show("Tekst został zapisany");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            StreamReader plik = new StreamReader("plik.txt");
            richTextBox1.Text = plik.ReadToEnd();
            plik.Close();
        }
    }
}
