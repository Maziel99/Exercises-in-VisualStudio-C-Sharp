﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace WindowsFormsApp5
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        OpenFileDialog oknoOtwierania = new OpenFileDialog();
        SaveFileDialog oknoZapisu = new SaveFileDialog();
        FontDialog oknoCzcionki = new FontDialog();
        ColorDialog oknoKoloru = new ColorDialog();

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            StreamReader plik;
            oknoOtwierania.FileName = "plik.txt";
            oknoOtwierania.Filter = "Pliczki tekstowe(*.txt)|*.txt|Wszystkie pliczki(*.*)|*.*";
            if(oknoOtwierania.ShowDialog()==DialogResult.OK)
            {
                plik = new StreamReader(oknoOtwierania.FileName);
                richTextBox1.Text = plik.ReadToEnd();
                plik.Close(); 
            }


        }

        private void button2_Click(object sender, EventArgs e)
        {
            StreamWriter plik;
            oknoZapisu.FileName = "plik.txt";
            oknoZapisu.Filter= "Pliczki tekstowe(*.txt)|*.txt|Wszystkie pliczki(*.*)|*.*";
            if(oknoZapisu.ShowDialog()==DialogResult.OK)
            {
                plik = new StreamWriter(oknoZapisu.FileName);
                plik.Write(richTextBox1.Text);
                plik.Close();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            oknoCzcionki.ShowEffects = true;
            oknoCzcionki.ShowColor = true;
            if(oknoCzcionki.ShowDialog()==DialogResult.OK)
            {
                richTextBox1.Font = oknoCzcionki.Font;
                richTextBox1.ForeColor = oknoCzcionki.Color;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if(oknoKoloru.ShowDialog()==DialogResult.OK)
            {
                richTextBox1.ForeColor = oknoKoloru.Color;
            }
        }

        private void nowyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = "";
        }

        private void otwórzToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StreamReader plik;
            oknoOtwierania.FileName = "plik.txt";
            oknoOtwierania.Filter = "Pliczki tekstowe(*.txt)|*.txt|Wszystkie pliczki(*.*)|*.*";
            if (oknoOtwierania.ShowDialog() == DialogResult.OK)
            {
                plik = new StreamReader(oknoOtwierania.FileName);
                richTextBox1.Text = plik.ReadToEnd();
                plik.Close();
            }
        }
    }
}
